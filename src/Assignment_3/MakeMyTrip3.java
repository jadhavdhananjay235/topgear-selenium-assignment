package Assignment_3;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MakeMyTrip3 {
	public static void main(String[] args) throws Exception  {
		
		//Browser Initiation 
		System.setProperty("webdriver.chrome.driver", "E:\\testing software\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		driver.get("https://www.makemytrip.com/	");
		driver.manage().window().maximize();
		
		
		//Select Departure location and verify
		Thread.sleep(3000);
		driver.findElement(By.xpath("//div[@class='fsw_inputBox searchCity inactiveWidget ']/label/input")).sendKeys("Pune");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//div[@class='hsw_autocomplePopup autoSuggestPlugin ']/div/input")).sendKeys("Pune");
		
		List <WebElement> Options = driver.findElements(By.xpath("//ul[@class='react-autosuggest__suggestions-list']//p[@class='font14 appendBottom5 blackText']"));
		
		for(int i=0;i<Options.size();i++)
		{
			String text = Options.get(i).getText();
			System.out.println("From "+text);
			if(text.contains("Pune")) 
			{
				WebElement cs = Options.get(i);
				js.executeScript("arguments[0].click()", cs);
				break;
			}
		
	}

		//Select Destination Location and verify
		WebElement Distination = driver.findElement(By.xpath("//div[@class=\"hsw_autocomplePopup autoSuggestPlugin makeFlex column spaceBetween\"]/div/input"));
		js.executeScript("arguments[0].click()", Distination);
		Distination.sendKeys("Mumbai");
		List <WebElement> To = driver.findElements(By.xpath("//div/p[@class='font14 appendBottom5 blackText']"));
		
		for(int i=0;i<To.size();i++)
		{
			String text1 = To.get(i).getText();
			System.out.println("To "+text1);
			if(text1.contains("Mumbai")) 
			{
				WebElement ms = To.get(i);
				js.executeScript("arguments[0].click()", ms);
				break;
			}
		
	}
		
		
		//Calendar Selection
		Calendar calendar = Calendar.getInstance(); 
		String targetdate = "31-Jan-2022";
		SimpleDateFormat tagetDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Date formatedtagetDate;
		
			tagetDateFormat.setLenient(false);
			formatedtagetDate = tagetDateFormat.parse(targetdate);
			calendar.setTime(formatedtagetDate);
			
			int targetDay= calendar.get(Calendar.DAY_OF_MONTH);
			WebElement Date = driver.findElement(By.xpath("//div[@class=\"DayPicker-Day\"]//p[text()='"+targetDay+"']"));
			js.executeScript("arguments[0].click()", Date);
			
			
			String DateTime = driver.findElement(By.xpath("//div[@class=\"fsw_inputBox dates inactiveWidget \"]/label")).getText();
			System.out.println("Date:- "+DateTime); 
			
			
			
			//Click on search button
			WebElement SearchButton = driver.findElement(By.xpath("//a[@class='primaryBtn font24 latoBold widgetSearchBtn ']"));
			js.executeScript("arguments[0].click()", SearchButton);
			
			//New webpage search result
			WebElement Details = driver.findElement(By.xpath("(//button/span[@class='appendRight8'])[1]"));
			js.executeScript("arguments[0].click()", Details);
			
			//Book cheapest tickets(as it appears on 1st index)
			Thread.sleep(3000);
			WebElement BookNow = driver.findElement(By.xpath("(//div//button[@class=\"button corp-btn latoBlack buttonPrimary fontSize13  \"])[1]"));
			js.executeScript("arguments[0].click()", BookNow);
			
			//Print All review page details
			List<WebElement> Conformation = driver.findElements(By.xpath("//section[@class='flightDetailBlk']/div"));
			for(int i=0; i<Conformation.size(); i++) {
				String Review = Conformation.get(i).getText();
				System.out.println(Review);
			}
			
}
}



//Note:- It gives ElementClickInterceptedException. For that we can use javaScript for click operation.
