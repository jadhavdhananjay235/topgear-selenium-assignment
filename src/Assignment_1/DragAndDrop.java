package Assignment_1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class DragAndDrop {
	
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "E:\\testing software\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demoqa.com/droppable/");
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//button[@id='details-button']")).click(); //It's privacy notice
		driver.findElement(By.xpath("//a[@id='proceed-link']")).click();        //It's for proceed website
		
		WebElement Dragg = driver.findElement(By.xpath("//div[@id='draggable' and @class='drag-box mt-4 ui-draggable ui-draggable-handle']"));
		WebElement Drop = driver.findElement(By.xpath("//div[@id='droppable'   and@class='drop-box ui-droppable']"));
		Actions ac= new Actions(driver);
		ac.dragAndDrop(Dragg, Drop).build().perform();;
		
		String Result = Drop.getText();
		
		if(Result.equals("Dropped!")){
		System.out.println("Text present in box is " + "'" + Result + "'" );
		}
		else {
			System.out.println("Element not Dropped");
		}
		
		driver.quit();
	}

}
