package Assignment_1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Calender {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "E:\\testing software\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demoqa.com/date-picker");
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//button[@id='details-button']")).click(); //It's privacy notice
		driver.findElement(By.xpath("//a[@id='proceed-link']")).click();        //It's for proceed website
		
		Calendar calendar = Calendar.getInstance(); 
		String targetdate = "23-Feb-1998";
		String targetTime = "09:00";
		SimpleDateFormat tagetDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Date formatedtagetDate;
		try {
		tagetDateFormat.setLenient(false);
		formatedtagetDate = tagetDateFormat.parse(targetdate);
		calendar.setTime(formatedtagetDate);
		
		int targetDay= calendar.get(Calendar.DAY_OF_MONTH);
		int targetMonth = calendar.get(Calendar.MONTH);
		int targetyear = calendar.get(Calendar.YEAR);
		
		driver.findElement(By.id("dateAndTimePickerInput")).click();
		String actualDate = driver.findElement(By.xpath("//div[@class=\"react-datepicker__current-month react-datepicker__current-month--hasYearDropdown react-datepicker__current-month--hasMonthDropdown\"]")).getText();
		calendar.setTime(new SimpleDateFormat("MMM yyyy").parse(actualDate));
		
		int actualmonth = calendar.get(Calendar.MONTH);
		int actalyear = calendar.get(Calendar.YEAR);
		
		while(targetMonth < actualmonth || targetyear < actalyear) {
			driver.findElement(By.xpath("//button[@class='react-datepicker__navigation react-datepicker__navigation--previous']")).click();
			actualDate = driver.findElement(By.xpath("//div[@class=\"react-datepicker__current-month react-datepicker__current-month--hasYearDropdown react-datepicker__current-month--hasMonthDropdown\"]")).getText();
			calendar.setTime(new SimpleDateFormat("MMM yyyy").parse(actualDate));
			
			actualmonth = calendar.get(Calendar.MONTH);
			actalyear = calendar.get(Calendar.YEAR);
			
		}
		
		driver.findElement(By.xpath("//div[@class='react-datepicker__month']//div[text()='"+targetDay+"']")).click();
		
		driver.findElement(By.xpath("//ul[@class=\"react-datepicker__time-list\"]//li[text()='"+targetTime+"']")).click();
		
		
		String DateTime = driver.findElement(By.xpath("//div[@class='col-md-9 col-sm-12']")).getText();
		System.out.println(DateTime); 
		

		
		}
		catch(ParseException e){
			new Exception("Invalid date provided, Please check input date"); 
			
		}
		
		
		
	}

}
