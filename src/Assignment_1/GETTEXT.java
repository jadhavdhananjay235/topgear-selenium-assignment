package Assignment_1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class GETTEXT {
	
	public static void main(String[] args) throws InterruptedException 
	{
		System.setProperty("webdriver.chrome.driver", "E:\\testing software\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demoqa.com/selectable/");
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//button[@id='details-button']")).click(); //
		driver.findElement(By.xpath("//a[@id='proceed-link']")).click();
		
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//a[@id='close-fixedban']")).click();
		
		driver.findElement(By.xpath("//div[@id='demo-tabpane-list']/ul/li")).click();
		driver.findElement(By.xpath("//div[@id='demo-tabpane-list']/ul/li[text()=\"Dapibus ac facilisis in\"]")).click();
		driver.findElement(By.xpath("//div[@id='demo-tabpane-list']/ul/li[text()=\"Morbi leo risus\"]")).click();
		driver.findElement(By.xpath("//div[@id='demo-tabpane-list']/ul/li[text()=\"Porta ac consectetur ac\"]")).click();
		
		String ALL = driver.findElement(By.xpath("//ul[@id='verticalListContainer']")).getText();
		System.out.println("=====================================================================");
		System.out.println("Given List elements are as follows:- ");
		System.out.println(ALL);
		
		driver.quit();
		
	}

}
