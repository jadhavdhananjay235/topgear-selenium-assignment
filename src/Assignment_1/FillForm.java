package Assignment_1;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class FillForm {
	public static void main(String[] args) throws InterruptedException {
		
	
		System.setProperty("webdriver.chrome.driver", "E:\\testing software\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demoqa.com/automation-practice-form");
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//button[@id='details-button']")).click(); //It's privacy notice
		driver.findElement(By.xpath("//a[@id='proceed-link']")).click();        //It's for proceed website
		
		driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys("Dhananjay");
		driver.findElement(By.xpath("//input[@id='lastName']")).sendKeys("Jadhav");
		driver.findElement(By.xpath("//input[@id='userEmail']")).sendKeys("dhananjay@wipro.com");
		driver.findElement(By.xpath("(//div[@class='custom-control custom-radio custom-control-inline'])[1]")).click();
		driver.findElement(By.xpath("//input[@id='userNumber']")).sendKeys("1234567889");
		
		driver.findElement(By.xpath("//input[@id='dateOfBirthInput']")).click();
		
		
		WebElement Birthyear = driver.findElement(By.xpath("//select[@class='react-datepicker__year-select']"));
		Select sec = new Select(Birthyear);
		sec.selectByIndex(98);
		
		WebElement Birthmonth = driver.findElement(By.xpath("//select[@class='react-datepicker__month-select']"));
		Select ab = new Select(Birthmonth);
		ab.selectByVisibleText("February");
		
		driver.findElement(By.xpath("//div[@class='react-datepicker__day react-datepicker__day--023']")).click();
		
		driver.findElement(By.xpath("//img[@title=\"Ad.Plus Advertising\"]")).click();
		
		Actions act = new Actions(driver);
		WebElement SUBJECT = driver.findElement(By.xpath("//div[@class='css-1g6gooi']"));
		act.moveToElement(SUBJECT).click().build().perform();
		act.sendKeys("Maths").perform();
		act.sendKeys(Keys.ENTER).perform();
		
		
		driver.findElement(By.xpath("//div/label[text()='Sports']")).click();
		driver.findElement(By.xpath("//div/label[text()='Reading']")).click();
		driver.findElement(By.xpath("//div/label[text()='Music']")).click();
		
		driver.findElement(By.xpath("//div/textarea[@id='currentAddress']")).sendKeys("IDC2, Hinjewadi, Pune");
		
		driver.findElement(By.xpath("(//div[@aria-hidden='true' and @class=' css-tlfecz-indicatorContainer'])[1]")).click();;       
		
		driver.findElement(By.xpath("//div/button[@id='submit']")).click();
		
		String Details = driver.findElement(By.xpath("//div[@class='modal-content']")).getText();
		System.out.println(Details);
		
		driver.quit();
	}
	
}
		